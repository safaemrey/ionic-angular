import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AnasayfaComponent } from './anasayfa.component';
import { MyCreditsComponent } from '../my-credits/my-credits.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
    ],
    declarations: [AnasayfaComponent, MyCreditsComponent],
    providers: [
    ]
})
export class AnasayfaModule { }
