import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service'

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate() {
    console.log("guard içinddeym")
    if (this.authService.alicik == 'tarak') return true;
    else {
      this.router.navigateByUrl('/auth');
      return false;
    }

  }

}
