import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AnimationController } from '@ionic/angular';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  @ViewChild("route", { read: ElementRef, static: true }) route: ElementRef;

  constructor(private animationCtrl: AnimationController) {

  }

  ngOnInit() {
    const animation = this.animationCtrl.create()
      .addElement(this.route.nativeElement)
      .duration(500)
      .fromTo('opacity', '0', '1');
    console.log("auth121312 içindeym")
    animation.play()
    console.log("auth içindeym")
  }

}
