import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnasayfaComponent } from 'src/app/components/base/anasayfa/anasayfa.component';

import { BasePage } from './base.page';



const routes: Routes = [
  {
    path: '',
    component: BasePage,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'anasayfa'
      },
      {
        path: 'anasayfa',
        component: AnasayfaComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BasePageRoutingModule { }
