import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BasePageRoutingModule } from './base-routing.module';

import { BasePage } from './base.page';

import { PanelMenuModule } from 'primeng/panelmenu';




@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BasePageRoutingModule,
    PanelMenuModule,

  ],
  declarations: [BasePage],
  providers: [
  ]
})
export class BasePageModule {


}
