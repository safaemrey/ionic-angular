import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-base',
  templateUrl: './base.page.html',
  styleUrls: ['./base.page.scss'],
})
export class BasePage implements OnInit {
  constructor(
    public menu: MenuController
  ) { }
  items: MenuItem[];

  menuOpen() {

    if (this.menu.isOpen("main-menu")) {
      this.menu.close("main-menu");
      console.log("close yaragıı")
    }
    else this.menu.open("main-menu");
  }
  ngOnInit() {

    this.items = [
      {
        label: 'Anasayfa',
        icon: 'pi pi-pw pi-home',
      },
      {
        label: 'Kredilerim',
        icon: 'pi pi-pw pi-money-bill',
      },
      {
        label: 'Bilgilerim',
        icon: 'pi pi-pw pi-user',
      }
    ]
  }




}
